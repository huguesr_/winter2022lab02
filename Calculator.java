public class Calculator{
	public static int add(double n1, double n2){
		int result = (int)(n1+n2);
		return result;
	}
	public static int subtract(double n1, double n2){
		int result = (int)(n1-n2);
		return result;
	}
	public int multiply(double n1, double n2){
		int result = (int)(n1*n2);
		return result;
	}
	public double divide(double n1, double n2){
		double result = n1/n2;
		return result;
	}
}